<?php

/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA;
 * 
 */

namespace oat\taoDelivery\helper;

use common_session_SessionManager;
use krumo;
use oat\generis\model\data\ModelManager;

/**
 * Helper to render the delivery form on the group page
 * 
 * @author joel bout, <joel@taotesting.com>
 * @package taoDelivery
 
 */
class JSONData
{
    public static function setAttributeToTaoDeliveryTakeable($UserURI, $ExcuteDeliveryURI, $Attr){
        $persistence =  ModelManager::getModel()->getPersistence();
        $sql123 = "
            UPDATE test_taker_data
            SET TestTakerData=(SELECT JSON_REPLACE(
                    TestTakerData,
                    (
                        CONCAT(
                            SUBSTRING_INDEX(
                                JSON_UNQUOTE(
                                    JSON_SEARCH(
                                        TestTakerData,
                                        'one',
                                        '".$ExcuteDeliveryURI."'
                                    )
                                ),
                                '.',
                                3
                            ),
                            '.',
                            'TAO_DELIVERY_TAKABLE'
                        )
                    ),
                    ".$Attr."
                ))
            where TestTakerURI = '".$UserURI."';
    
        ";
        $results = $persistence->exec($sql123);

    }
    public static function getDeliveriesForUserURI($UserURI)
    {
        $persistence =  ModelManager::getModel()->getPersistence();

        $Deleiveries = array();
        $Deleiveries['Available'] = [];
        $Deleiveries['Resumable'] = [];


        $sql = "SELECT * FROM test_taker_data WHERE TestTakerURI = '" . $UserURI . "'";
        $results = $persistence->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
        $results = $results[0];

        $redis = new \Redis([
            'host' => '127.0.0.1',
            'port' => 6379,
            'connectTimeout' => 2.5
        ]);
        
        
        $results = $redis->get($UserURI);
        if ($results != null) {
    
            $Data = json_decode($results, true);
    
            if (count($Data[0]['delivery']) > 0) {
                foreach ($Data[0]['delivery'] as $element) {

                    if ($element['label'] != null) {

                        $DeliveryFromAssemblyRow = $element;
                        $DeliverExcutionCounter = $element['DeliveryExecutionDelivery'] == null ? 0 : count($element['DeliveryExecutionDelivery']);
                        if (($element['start_date'] <= time() &&  $element['end_date'] > time()) ||
                            ($element['start_date'] <= time() &&  $element['end_date'] == null) ||
                            ($element['start_date'] == null &&  $element['end_date'] > time()) || 
                            ($element['start_date'] == null &&  $element['end_date']== null)) {
        
                            if ($element['max_exec'] > $DeliverExcutionCounter || $element['max_exec'] == null) {

                                $Deleiveries['Available'][] = JSONData::buildFromAssembly($DeliveryFromAssemblyRow);
                            }

                            if ($element['DeliveryExecutionDelivery'] != null) {
                                foreach ($element['DeliveryExecutionDelivery'] as $DeliveryExecutionDeliveryElement) {
                                    if ($DeliveryExecutionDeliveryElement['TAO_DELIVERY_TAKABLE'] == 1) {
                                        $Deleiveries['Resumable'][] = JSONData::buildFromDeliveryExecution($DeliveryExecutionDeliveryElement);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $Deleiveries;
    }

    public static function buildFromAssembly($DeliveryFromAssemblyRow)
    {
        return array(
            'id'=> $DeliveryFromAssemblyRow['uri'],
            'label' =>$DeliveryFromAssemblyRow['label'],
            'launchUrl' => _url('runDeliveryExecution', 'DeliveryServer', null, array('deliveryExecution' => $DeliveryFromAssemblyRow['uri'])),
            'description' => [],
            'TAO_DELIVERY_TAKABLE' => true
        );
    }

    public static function buildFromDeliveryExecution($DeliveryExecutionDeliveryElement)
    {
        return array(
            'id'=> $DeliveryExecutionDeliveryElement['uri'],
            'label' =>$DeliveryExecutionDeliveryElement['label'],
            'launchUrl' => _url('runDeliveryExecution', 'DeliveryServer', null, array('deliveryExecution' => $DeliveryExecutionDeliveryElement['uri'])),
            'description' => [$DeliveryExecutionDeliveryElement['description']],
            'TAO_DELIVERY_TAKABLE' => true
        );
    }
}
