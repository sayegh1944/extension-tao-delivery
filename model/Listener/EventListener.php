<?php

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2021 (original work) Open Assessment Technologies SA;
 */


namespace oat\taoDelivery\model\Listener;

use core_kernel_classes_Class;
use oat\generis\model\data\event\ResourceCreated;
use oat\generis\model\OntologyAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use oat\generis\model\OntologyRdfs;

use core_kernel_classes_Resource;
use core_kernel_classes_Property;

use oat\tao\model\TaoOntology;


use oat\generis\model\data\event\ResourceDeleted;
use oat\generis\model\data\event\ResourceUpdated;

use oat\taoTests\models\event\TestChangedEvent;
use oat\taoTests\models\event\TestCreatedEvent;
use oat\taoTests\models\event\TestDuplicatedEvent;
use oat\taoTests\models\event\TestExecutionPausedEvent;
use oat\taoTests\models\event\TestExecutionResumedEvent;
use oat\taoTests\models\event\TestImportEvent;
use oat\taoTests\models\event\TestUpdatedEvent;

use oat\taoDeliveryRdf\model\event\AbstractDeliveryEvent;
use oat\taoDeliveryRdf\model\event\DeliveryCreatedEvent;
use oat\taoDeliveryRdf\model\event\DeliveryRemovedEvent;
use oat\taoDeliveryRdf\model\event\DeliveryUpdatedEvent;

use oat\taoOutcomeUi\model\event\ResultsListPluginEvent;

use oat\taoProctoring\model\event\DeliveryExecutionExpired;
use oat\taoProctoring\model\event\DeliveryExecutionFinished;
use oat\taoProctoring\model\event\DeliveryExecutionIrregularityReport;
use oat\taoProctoring\model\event\DeliveryExecutionTerminated;

use oat\taoQtiItem\model\event\ItemCreatorLoad;
use oat\taoQtiItem\model\event\ItemImported;
use oat\taoQtiItem\model\event\QtiItemExportEvent;
use oat\taoQtiItem\model\event\QtiItemImportEvent;
use oat\taoQtiItem\model\event\QtiItemMetadataExportEvent;

use oat\taoTestTaker\models\events\AbstractTestTakerEvent;
use oat\taoTestTaker\models\events\TestTakerClassCreatedEvent;
use oat\taoTestTaker\models\events\TestTakerClassRemovedEvent;
use oat\taoTestTaker\models\events\TestTakerCreatedEvent;
use oat\taoTestTaker\models\events\TestTakerExportedEvent;
use oat\taoTestTaker\models\events\TestTakerUpdatedEvent;

class EventListener  
{
    use OntologyAwareTrait;
    use ServiceLocatorAwareTrait;

    public static function listen($event)
    {
        if (
            $event instanceof TestChangedEvent ||
            $event instanceof TestCreatedEvent ||
            $event instanceof TestDuplicatedEvent ||
            $event instanceof TestExecutionPausedEvent ||
            $event instanceof TestExecutionResumedEvent ||
            $event instanceof TestUpdatedEvent ||

            $event instanceof DeliveryCreatedEvent ||
            $event instanceof DeliveryUpdatedEvent ||

            $event instanceof TestTakerCreatedEvent ||
            $event instanceof TestTakerUpdatedEvent 
            ) {
            EventListener::UpdateDataTable();
        }
        if (
            $event instanceof DeliveryRemovedEvent
            ) {
            EventListener::UpdateDataTableAfterRemoveDeleivery();
        }

    }

    public static function UpdateDataTable()
    {
            $persistence =  \common_persistence_Manager::getPersistence('default');

            $Query = "CALL `delete_delivery_association`();";

            $persistence->exec($Query);

            $Query = "CALL `update_test_taker_data`();";


            $persistence->exec($Query);

    
            $sql = "SELECT * FROM test_taker_data";
            $results = $persistence->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

            $redis = new \Redis([
                'host' => '127.0.0.1',
                'port' => 6379,
                'connectTimeout' => 2.5
            ]);
            $redis->flushAll();

            foreach ($results as $result) {
                $redis->set($result['TestTakerURI'], $result['TestTakerData'] );
            }
    }

    public static function UpdateDataTableAfterRemoveDeleivery()
    {
            $persistence =  \common_persistence_Manager::getPersistence('default');
            $Query = "CALL `delete_undelivered_delivery_execution_delivery`();";

            EventListener::UpdateDataTable();
    }
}
