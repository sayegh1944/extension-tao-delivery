<?php

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2021 (original work) Open Assessment Technologies SA;
 */

namespace oat\taoDelivery\scripts\install;

use oat\generis\model\data\event\ResourceCreated;
use oat\generis\model\data\event\ResourceDeleted;
use oat\generis\model\data\event\ResourceUpdated;

use oat\taoTests\models\event\TestChangedEvent;
use oat\taoTests\models\event\TestCreatedEvent;
use oat\taoTests\models\event\TestDuplicatedEvent;
use oat\taoTests\models\event\TestExecutionPausedEvent;
use oat\taoTests\models\event\TestExecutionResumedEvent;
use oat\taoTests\models\event\TestImportEvent;
use oat\taoTests\models\event\TestUpdatedEvent;

use oat\taoDeliveryRdf\model\event\AbstractDeliveryEvent;
use oat\taoDeliveryRdf\model\event\DeliveryCreatedEvent;
use oat\taoDeliveryRdf\model\event\DeliveryRemovedEvent;
use oat\taoDeliveryRdf\model\event\DeliveryUpdatedEvent;

use oat\taoOutcomeUi\model\event\ResultsListPluginEvent;

use oat\taoProctoring\model\event\DeliveryExecutionExpired;
use oat\taoProctoring\model\event\DeliveryExecutionFinished;
use oat\taoProctoring\model\event\DeliveryExecutionIrregularityReport;
use oat\taoProctoring\model\event\DeliveryExecutionTerminated;

use oat\taoQtiItem\model\event\ItemCreatorLoad;
use oat\taoQtiItem\model\event\ItemImported;
use oat\taoQtiItem\model\event\QtiItemExportEvent;
use oat\taoQtiItem\model\event\QtiItemImportEvent;
use oat\taoQtiItem\model\event\QtiItemMetadataExportEvent;

use oat\taoTestTaker\models\events\AbstractTestTakerEvent;
use oat\taoTestTaker\models\events\TestTakerClassCreatedEvent;
use oat\taoTestTaker\models\events\TestTakerClassRemovedEvent;
use oat\taoTestTaker\models\events\TestTakerCreatedEvent;
use oat\taoTestTaker\models\events\TestTakerExportedEvent;
use oat\taoTestTaker\models\events\TestTakerUpdatedEvent;

use oat\oatbox\event\EventManager;
use oat\oatbox\extension\InstallAction;
use oat\taoDelivery\model\Listener\EventListener;

class RegisterEvents extends InstallAction
{
    public function __invoke($params)
    {
        $eventManager = $this->getServiceManager()->get(EventManager::SERVICE_ID);

        $ClassArray = [
            TestChangedEvent::class,
            TestCreatedEvent::class,
            TestDuplicatedEvent::class,
            TestExecutionPausedEvent::class,
            TestExecutionResumedEvent::class,
            TestUpdatedEvent::class,

            DeliveryCreatedEvent::class,
            DeliveryRemovedEvent::class,
            DeliveryUpdatedEvent::class,

            TestTakerCreatedEvent::class,
            TestTakerUpdatedEvent::class
        ];

        
        foreach ($ClassArray as $ClassElement) {
            $eventManager->attach(
                $ClassElement,
                [
                    EventListener::class,
                    'listen'
                ]
            );
        }



        $this->getServiceManager()->register(EventManager::SERVICE_ID, $eventManager);
    }
}
