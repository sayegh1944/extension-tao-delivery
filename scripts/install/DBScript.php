<?php

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2021 (original work) Open Assessment Technologies SA;
 */

namespace oat\taoDelivery\scripts\install;

use oat\oatbox\extension\InstallAction;

use oat\generis\model\OntologyAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class DBScript extends InstallAction
{
    use OntologyAwareTrait;
    use ServiceLocatorAwareTrait;


    public function __invoke($params)
    {
        $persistence = $this->getModel()->getPersistence();

        $Query = "
            DELIMITER $$
            CREATE DEFINER=`root`@`localhost` PROCEDURE `update_test_taker_data`()
            BEGIN
            TRUNCATE TABLE test_taker_data;
                            
            INSERT INTO `test_taker_data`(`TestTakerURI`, `TestTakerData`) 
            SELECT DISTINCT `s1`.`subject` AS `TestTakerURI`,
            (
                select json_arrayagg(
                        json_object(
                            'label',
                            (
                                select `s3`.`object`
                                from `statements` `s3`
                                where (
                                        (
                                            `s3`.`predicate` = 'http://www.w3.org/2000/01/rdf-schema#label'
                                        )
                                        and (`s3`.`subject` = `s2`.`object`)
                                    )
                            ),
                            'uri',
                            `s2`.`object`,
                            'delivery',
                            (
                                select json_arrayagg(
                                        json_object(
                                            'label',
                                            @delivery_label := (
                                                select `s5`.`object`
                                                from `statements` `s5`
                                                where (
                                                        (
                                                            `s5`.`predicate` = 'http://www.w3.org/2000/01/rdf-schema#label'
                                                        )
                                                        and (`s5`.`subject` = `s4`.`object`)
                                                    )
                                            ),
                                            'max_exec',
                                            (
                                                select `10`.`object`
                                                from `statements` `10`
                                                where (
                                                        (
                                                            `10`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#Maxexec'
                                                        )
                                                        and (`10`.`subject` = `s4`.`object`)
                                                    )
                                            ),
        
                                            'start_date',
                                            (
                                                select `11`.`object`
                                                from `statements` `11`
                                                where (
                                                        (
                                                            `11`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodStart'
                                                        )
                                                        and (`11`.`subject` = `s4`.`object`)
                                                    )
                                            ),
        
                                            'end_date',
                                            (
                                                select `10`.`object`
                                                from `statements` `10`
                                                where (
                                                        (
                                                            `10`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodEnd'
                                                        )
                                                        and (`10`.`subject` = `s4`.`object`)
                                                    )
                                            ),
        
                                            'uri',
                                            `s4`.`object`,
                                            'DeliveryExecutionDelivery',
                                            IF(
                                                @delivery_label IS NULL,
                                                NULL,
                                                (
                                                    SELECT JSON_ARRAYAGG(
                                                            JSON_OBJECT(
                                                                'label',
                                                                (
                                                                    SELECT s7.object
                                                                    FROM statements s7
                                                                    WHERE predicate = 'http://www.w3.org/2000/01/rdf-schema#label'
                                                                        AND s7.subject = s6.subject
                                                                ),
                                                                'uri',
                                                                s6.subject,
                                                                'TAO_DELIVERY_TAKABLE',
                                                                (
                                                                    SELECT CASE
                                                                            WHEN (
                                                                                s8.object = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusActive'
                                                                            ) THEN TRUE
                                                                            WHEN (
                                                                                s8.object = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusPaused'
                                                                            ) THEN TRUE
                                                                            ELSE FALSE
                                                                        END AS QuantityText
                                                                    FROM statements s8
                                                                    WHERE predicate = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#StatusOfDeliveryExecution'
                                                                        AND s8.subject = s6.subject
                                                                ),
                                                                'description',
                                                                (
                                                                    SELECT FROM_UNIXTIME(
                                                                            SUBSTRING_INDEX(s9.object, ' ', -1),
                                                                            'Started at %d/%m/%Y %h:%i:%s'
                                                                        )
                                                                    FROM statements s9
                                                                    WHERE predicate = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStart'
                                                                        AND s9.subject = s6.subject
                                                                )
                                                            )
                                                        )
                                                    FROM statements s6
                                                    WHERE s6.object = s4.object
                                                        AND predicate = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionDelivery'
                                                )
                                            )
                                        )
                                    )
                                from `statements` `s4`
                                where (
                                        (`s4`.`subject` = `s2`.`object`)
                                        and (
                                            `s4`.`predicate` = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries'
                                        )
                                    )
                            )
                        )
                    )
                from `statements` `s2`
                where (
                        (`s2`.`subject` = `s1`.`subject`)
                        and (
                            `s2`.`predicate` = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#member'
                        )
                    )
            ) AS `TestTakerData`
            FROM `statements` AS `s1`
            WHERE `s1`.`subject` in (
                    select distinct `s0`.`subject`
                    from `statements` `s0`
                    where (
                            (
                                `s0`.`object` = 'http://www.tao.lu/Ontologies/TAOSubject.rdf#Subject'
                            )
                            AND (
                                `s0`.`predicate` = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
                            )
                        )
                );        
                END$$
                DELIMITER ;
        ";

        $persistence->exec($Query);

        $Query = "
        DELIMITER $$
            CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_delivery_association`()
            DELETE FROM `statements`
            WHERE `object` IN (
                    SELECT s
                    FROM (
                            SELECT DISTINCT(`object`) AS s
                            FROM `statements`
                            WHERE `predicate` = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries'
                                AND `object` NOT IN (
                                    SELECT DISTINCT(`subject`)
                                    FROM `statements`
                                    WHERE `predicate` = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
                                    AND `object` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery'
                                )
                        )  AS myStatementsAlias
                )$$
            DELIMITER ;
        ";

        $persistence->exec($Query);

        $Query = "
            DELIMITER $$
            CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_undelivered_delivery_execution_delivery`()
            DELETE E3
            FROM `statements` E3
            WHERE `E3`.`subject` IN (
                    SELECT s
                    FROM (
                            SELECT DISTINCT(`E1`.`subject`) AS S
                            FROM `statements` E1
                            WHERE `E1`.`predicate` = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
                                AND `E1`.`object` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecution'
                                AND `E1`.`subject` NOT IN (
                                    SELECT DISTINCT(`E2`.`subject`)
                                    FROM `statements` `E2`
                                    WHERE `E2`.`predicate` = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
                                        AND `E2`.`object` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery'
                                )
                        ) AS myStatementsAlias
                )$$
            DELIMITER ;";

        $persistence->exec($Query);




        $Query = "
            CREATE TABLE IF NOT EXISTS `impoted_data_from_to_table_user_data` (
            `TestTakerData` json DEFAULT NULL,
            `id` int NOT NULL,
            `TestTakerURI` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

            ALTER TABLE `impoted_data_from_to_table_user_data`
            ADD PRIMARY KEY (`id`),
            ADD KEY `TestTakerURI` (`TestTakerURI`);

            ALTER TABLE `impoted_data_from_to_table_user_data`
            MODIFY `id` int NOT NULL AUTO_INCREMENT;
        ";

        $persistence->exec($Query);
    }
}
